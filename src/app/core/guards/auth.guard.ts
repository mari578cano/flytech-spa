import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {map, Observable} from 'rxjs';

// Services
import { AccountService } from "../../services/account.service";
import {ToastrService} from "ngx-toastr";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private accountService: AccountService, private toastr: ToastrService, private router: Router) {
  }
  canActivate(): Observable<boolean> {
    return this.accountService.currentUser$.pipe(
      map<any, any>(user => {
        if (!user) {
          this.router.navigateByUrl('/login');
          this.toastr.error('Acceso denegado!!');
          return false;
        } else {
          return true;
        }
      })
    );
  }

}
