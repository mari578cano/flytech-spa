export interface Collection {
  id: number;
  dateCreated: string;
  station: string;
  sense: string;
  hour: string;
  category: string;
  amount: string;
  tabulatedValue: number;
}
