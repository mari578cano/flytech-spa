export interface Filters {
  station?: string;
  sense?: string;
  category?: string;
}
