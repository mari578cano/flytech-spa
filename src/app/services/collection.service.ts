import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

// Environment
import { environment } from "../../environments/environment";
import {Filters} from "../models/Filters";


@Injectable({
  providedIn: 'root'
})
export class CollectionService {

  baseUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }

  public uploadDataAmount(date) {
    return this.http.get(`${this.baseUrl}collection/AmountCarsExternal/${date}`);
  }

  public uploadDataCollection(date) {
    return this.http.get(`${this.baseUrl}collection/CollectionCarsExternal/${date}`);
  }

  public getDataCollection(filters?: Filters) {
    const _filters = Object.entries(filters)
      .filter((f) => f[1] != '' || f[1] != null)
      .map((f) => `${f[0]}=${f[1]}&`)
      .reduce( (acc: string, value: string) => acc + value, '');
    return this.http.get(`${this.baseUrl}collection/CollectionFilters?${_filters}`);
  }

  public getDataReport() {
    return this.http.get(`${this.baseUrl}ReportCollection/GetReport`);
  }
}
