import { Injectable } from '@angular/core';
import {map, Observable, ReplaySubject} from "rxjs";
import {HttpClient} from "@angular/common/http";

// Models
import { User } from '../models/user'

// Environment
import { environment } from "../../environments/environment";



@Injectable({
  providedIn: 'root'
})
export class AccountService {
  baseUrl = environment.apiUrl;
  private currentUserSource = new ReplaySubject<User | null>(1);
  currentUser$ = this.currentUserSource.asObservable();

  constructor(private http: HttpClient) { }

  public login(username: string, password: string): Observable<User> {
    return this.http.post<User>(`${this.baseUrl}account/login`, {username, password}).pipe(
      map<User,any>((user: User) => {
        if (user) {
          localStorage.setItem('user', JSON.stringify(user));
          this.currentUserSource.next(user);
        }
      })
    );
  }

  setCurrentUser(user: User): void {
    this.currentUserSource.next(user);
  }

  logout(): void{
    localStorage.removeItem('user');
    this.currentUserSource.next(null);
  }

  emitUser(value) {
    this.currentUserSource.next(value);
  }

}
