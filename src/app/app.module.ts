import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import { AppComponent } from './app.component';
import {RouterModule} from "@angular/router";


// Modules
import {AuthModule} from "./feature/auth/auth.module";
import {ToastrModule} from 'ngx-toastr';

// Routing
import {AppRoutingModule} from "./app-routing.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {JwtInterceptor} from "./interceptor/jwt.interceptor";

@NgModule({
  declarations: [
    AppComponent
  ],
    imports: [
      BrowserModule,
      RouterModule,
      AuthModule,
      AppRoutingModule,
      HttpClientModule,
      BrowserAnimationsModule,
      ToastrModule.forRoot({
        positionClass: 'toast-bottom-right'
      }),
    ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}
  ],
  bootstrap: [AppComponent],
  exports: [
    ToastrModule
  ]
})
export class AppModule { }
