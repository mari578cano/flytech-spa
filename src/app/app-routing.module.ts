import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

// guards
import { AuthGuard } from "./core/guards/auth.guard";

const routes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {
    path        : '',
    loadChildren: () => import('./feature/collection/collection.module').then(m => m.CollectionModule),
    canActivate: [AuthGuard]
  },
  {
    path: '**',
    redirectTo: '/collection'
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
