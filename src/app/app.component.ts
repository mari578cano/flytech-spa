import { Component } from '@angular/core';
import {AccountService} from "./services/account.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Flytech-SPA';

  constructor(private accountService: AccountService) {
    this.accountService.emitUser(null);
  }
}


