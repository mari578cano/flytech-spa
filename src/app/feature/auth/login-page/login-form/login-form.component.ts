import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";

// Service
import {ToastrService} from "ngx-toastr";
import {AccountService} from "../../../../services/account.service";


@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  public form: FormGroup;

  constructor(public accountService: AccountService, private router: Router, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.formLogin();
  }

  private formLogin(): void {
    this.form = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    });
  }


  public login(): void {
    this.accountService.login(this.form.value.username, this.form.value.password).subscribe( response => {
      this.router.navigateByUrl('/collection');
    }, error => {
      this.toastr.error(error.error);
    });
  }

  public logout(): void {
    this.accountService.logout();
    this.router.navigateByUrl('/');
  }

}
