import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Components
import { LoginPageComponent} from "./login-page/login-page.component";

// Routing
import {AuthRoutingModule} from "./auth-routing.module";
import { LoginFormComponent } from './login-page/login-form/login-form.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    LoginPageComponent,
    LoginFormComponent
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class AuthModule { }
