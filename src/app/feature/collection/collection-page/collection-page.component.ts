import { Component, OnInit } from '@angular/core';

// Service
import {CollectionService} from "../../../services/collection.service";
import {ToastrService} from "ngx-toastr";

// Models
import {Collection} from "../../../models/collection";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-collection-page',
  templateUrl: './collection-page.component.html',
  styleUrls: ['./collection-page.component.css']
})
export class CollectionPageComponent implements OnInit {

  collection: Collection[];
  public formCollection: FormGroup;
  constructor(public collectionService: CollectionService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.getData();
    this.formLogin();
  }

  private formLogin(): void {
    this.formCollection = new FormGroup({
      station: new FormControl(''),
      sense: new FormControl(''),
      category: new FormControl('')
    });
  }

  public getData() {
  this.collectionService.getDataCollection({...this.formCollection?.value})
    .subscribe((res: Collection[]) => {
      this.collection = res;
    }, error => {
       this.toastr.error('Ocurrio un error');
    })
  }

}
