import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

// Components
import {CollectionPageComponent} from "./collection-page/collection-page.component";
import {CollectionReportPageComponent} from "./collection-report-page/collection-report-page.component";
import {CollectionComponent} from "./collection.component";
import {UploadCollectionsComponent} from "./upload-collections/upload-collections.component";


const routes: Routes = [
  {
    path: '',
    component: CollectionComponent,
    children: [
      {
        path     : 'collection',
        component: CollectionPageComponent
      },
      {
        path     : 'collection-report',
        component: CollectionReportPageComponent
      },
      {
        path     : 'upload-collections',
        component: UploadCollectionsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CollectionRoutingModule {
}
