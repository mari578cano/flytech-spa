import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

// Service
import {ToastrService} from "ngx-toastr";
import {CollectionService} from "../../../services/collection.service";

@Component({
  selector: 'app-upload-collections',
  templateUrl: './upload-collections.component.html',
  styleUrls: ['./upload-collections.component.css']
})
export class UploadCollectionsComponent implements OnInit {
  public form: FormGroup;

  constructor(public collectionService: CollectionService, private router: Router, private toastr: ToastrService) {
  }

  ngOnInit(): void {
    this.formLogin();
  }

  private formLogin(): void {
    this.form = new FormGroup({
      date: new FormControl('', Validators.required),
      type: new FormControl('recaudo', Validators.required),
    });
  }

  public uploadData(): void {
    if(this.form.value.type == 'conteo')
    {
      this.collectionService.uploadDataAmount(this.form.value.date).subscribe((res) => {
        this.toastr.success('Los datos fueron cargados correctamente.')
      }, error => {
        this.toastr.error('Ocurrio un error!! intentelo nuevamente.')
      });
    } else {
      this.collectionService.uploadDataCollection(this.form.value.date).subscribe((res) => {
        this.toastr.success('Los datos fueron cargados correctamente.')
      }, error => {
        this.toastr.error('Ocurrio un error!! intentelo nuevamente.')
      });
    }
  }

}

