import { Component, OnInit } from '@angular/core';
import {CollectionService} from "../../../services/collection.service";
import {Collection} from "../../../models/collection";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-collection-report-page',
  templateUrl: './collection-report-page.component.html',
  styleUrls: ['./collection-report-page.component.css']
})
export class CollectionReportPageComponent implements OnInit {

  public data: any;
  constructor(public collectionService: CollectionService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.getDataReport();
  }

  public getDataReport() {
    this.collectionService.getDataReport()
      .subscribe((res: any) => {
        console.log(res);
        this.data = res;
      }, error => {
        this.toastr.error('Ocurrio un error');
      })
  }

}
