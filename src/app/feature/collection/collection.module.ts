import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";

// Routing
import {CollectionRoutingModule} from "./collection-routing.module";

// Components
import { CollectionPageComponent } from './collection-page/collection-page.component';
import { CollectionReportPageComponent } from './collection-report-page/collection-report-page.component';
import { CollectionComponent } from './collection.component';
import { UploadCollectionsComponent } from './upload-collections/upload-collections.component';

// Module
import {NavModule} from "../../shared/components/nav/nav.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    CollectionPageComponent,
    CollectionReportPageComponent,
    CollectionComponent,
    UploadCollectionsComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    CollectionRoutingModule,
    NavModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class CollectionModule { }
